﻿using Microsoft.AspNetCore.Identity;

namespace Phone_Accessories.Models.Entity
{
    public class AppUser : IdentityUser
    {
        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public byte[]? ProfilePicture { get; set; }

        public Cart? Cart { get; set; }
    }
}