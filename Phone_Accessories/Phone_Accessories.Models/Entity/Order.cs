﻿using Phone_Accessories.Models.BaseEntity;

namespace Phone_Accessories.Models.Entity
{
    public class Order : EntityBase
    {
        public string? NameUser { get; set; }

        public string? Address { get; set; }

        public string? Phone { get; set; }

        public bool? Status { get; set; }

        public string? UserId { get; set; }

        public IEnumerable<OrderDetail>? OrderDetails { get; set; }
    }
}