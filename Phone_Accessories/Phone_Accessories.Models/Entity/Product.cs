﻿using Phone_Accessories.Models.BaseEntity;

namespace Phone_Accessories.Models.Entity
{
    public class Product : EntityBase
    {
        public string? Name { get; set; }

        public string? Description { get; set; }

        public byte[]? Image { get; set; }

        public int ColorId { get; set; }

        public Color? Color { get; set; }

        public int? SizeId { get; set; }

        public Size? Size { get; set; }

        public int Quantity { get; set; }

        public int Sold { get; set; }

        public decimal Price { get; set; }

        public int PromotionPrice { get; set; }

        public bool Status { get; set; }

        public int CategoryId { get; set; }

        public Category? Category { get; set; }

        public IEnumerable<OrderDetail>? OrderDetails { get; set; }

        public IEnumerable<CartDetail>? CartDetail { get; set; }
    }
}