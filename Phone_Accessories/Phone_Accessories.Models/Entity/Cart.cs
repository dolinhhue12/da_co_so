﻿using Phone_Accessories.Models.BaseEntity;

namespace Phone_Accessories.Models.Entity
{
    public class Cart : EntityBase
    {
        public IEnumerable<CartDetail>? CartDetail { get; set; }

        public string? UserId { get; set; }

        public AppUser? AppUser { get; set; }
    }
}