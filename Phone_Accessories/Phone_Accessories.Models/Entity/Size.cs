﻿using Phone_Accessories.Models.BaseEntity;

namespace Phone_Accessories.Models.Entity
{
    public class Size : EntityBase
    {
        public string? Name { get; set; }

        public IEnumerable<Product>? Products { get; set; }
    }
}