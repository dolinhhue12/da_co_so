﻿using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Data.IRepositories;
using Phone_Accessories.Models.Entity;

namespace Phone_Accessories.Data.Repositories
{
    public class CartDetailRepository : GenericRepository<CartDetail>, ICartDetailRepository
    {
        public CartDetailRepository(ApplicationDbContext context) : base(context)
        {
        }

        public void DeleteVer1(CartDetail cart)
        {
            this.DbSet.Remove(cart);
        }
    }
}