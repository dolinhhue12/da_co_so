﻿using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Data.IRepositories;
using Phone_Accessories.Models.Entity;

namespace Phone_Accessories.Data.Repositories
{
    public class SizeRepository : GenericRepository<Size>, ISizeRepository
    {
        public SizeRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}