﻿using Microsoft.EntityFrameworkCore;
using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Data.IRepositories;
using Phone_Accessories.Models.Entity;

namespace Phone_Accessories.Data.Repositories
{
    public class CartRepository : GenericRepository<Cart>, ICartRepository
    {
        public CartRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<Cart> GetCart(string userId)
        {
            return await this.DbSet.FirstOrDefaultAsync(x => x.UserId.Equals(userId));
        }
    }
}