﻿using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Data.IRepositories;
using Phone_Accessories.Models.Entity;

namespace Phone_Accessories.Data.Repositories
{
    public class ColorRepository : GenericRepository<Color>, IColorReporitory
    {
        public ColorRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}