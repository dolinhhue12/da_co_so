﻿using Phone_Accessories.ViewModels.IdentityResult;
using Phone_Accessories.ViewModels.UserRoles;

namespace Phone_Accessories.Application.Users
{
    public interface IUserService
    {
        Task<IdentityCustomResult> CreateAsync(CreateUserVm request);

        Task<IdentityCustomResult> SignInAsync(LoginVm request);

        Task<IdentityCustomResult> RoleAssignAsync(string email, string roleName);

        Task<IdentityCustomResult> GenerateToken(LoginVm request);

        Task<IList<UserRolesViewModel>> GetUsers();

        Task<IList<ManageUserRolesViewModel>> GetRoles(string userId);

        Task<bool> UpdateRoleUser(IList<ManageUserRolesViewModel> model, string userId);
    }
}