﻿using AutoMapper;
using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Models.Entity;
using Phone_Accessories.ViewModels.Carts;
using Phone_Accessories.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phone_Accessories.Application.Carts
{
    public class CartService : ICartService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public CartService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<bool> Create(CartVm createCart)
        {
            try
            {
                var cart = await this.unitOfWork.CartRepository.GetCart(createCart.UserId);

                var cartDetail = new CartDetail()
                {
                    CartId = cart.Id,
                    ProductId = createCart.ProductId,
                    Quantity = createCart.Quantity,
                };

                await this.unitOfWork.CartDetailRepository.Add(cartDetail);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Delete(string userId, int productId)
        {
            try
            {
                var cart = await this.unitOfWork.CartRepository.GetCart(userId);

                var cartDetails = await this.unitOfWork.CartDetailRepository.Find(x => x.CartId == cart.Id);

                foreach (var item in cartDetails)
                {
                    if (item.ProductId == productId)
                        this.unitOfWork.CartDetailRepository.DeleteVer1(item);
                }

                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch
            {

                return false;
            }

        }

        public async Task<IList<ViewProduct>> GetCart(string userId)
        {
            var cart = await this.unitOfWork.CartRepository.GetCart(userId);

            if (cart == null)
            {
                return null;
            }

            var cartDetails = await this.unitOfWork.CartDetailRepository.Find(x => x.CartId == cart.Id);
            var products = new List<ViewProduct>();

            foreach (var cartDetail in cartDetails)
            {
                var product = mapper.Map<ViewProduct>(await this.unitOfWork.ProductRepository.GetByIdInclude(cartDetail.ProductId));
                product.Quantity = cartDetail.Quantity;
                products.Add(product);
            }

            return products;
        }

        public Task<bool> Update(CartVm createCart)
        {
            throw new NotImplementedException();
        }
    }
}
