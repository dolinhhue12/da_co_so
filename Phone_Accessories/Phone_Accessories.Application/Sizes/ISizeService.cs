﻿using Phone_Accessories.ViewModels.Sizes;

namespace Phone_Accessories.Application.Sizes
{
    public interface ISizeService
    {
        Task<IList<SizeVm>> GetAll();

        Task<bool> Create(SizeVm categoryVm);

        Task<SizeVm> GetById(int id);

        Task<bool> Update(SizeVm categoryVm);

        Task<bool> Delete(int id);

        Task<IList<SizeVm>> Search(string categoryName);
    }
}