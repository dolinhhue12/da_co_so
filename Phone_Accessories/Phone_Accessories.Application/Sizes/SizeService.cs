﻿using AutoMapper;
using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Models.Entity;
using Phone_Accessories.ViewModels.Sizes;

namespace Phone_Accessories.Application.Sizes
{
    public class SizeService : ISizeService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public SizeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<bool> Create(SizeVm categoryVm)
        {
            try
            {
                var category = mapper.Map<Size>(categoryVm);

                await this.unitOfWork.SizeRepository.Add(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                unitOfWork.SizeRepository.Delete(id);
                await unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<SizeVm>> GetAll()
        {
            var categories = await this.unitOfWork.SizeRepository.GetAll();
            var cagoryVms = new List<SizeVm>();

            foreach (var category in categories)
            {
                cagoryVms.Add(mapper.Map<SizeVm>(category));
            }
            return cagoryVms;
        }

        public async Task<SizeVm> GetById(int id)
        {
            var category = await unitOfWork.SizeRepository.GetById(id);
            var categoryVm = mapper.Map<SizeVm>(category);

            return categoryVm;
        }

        public async Task<IList<SizeVm>> Search(string categoryName)
        {
            var categories = await this.unitOfWork.SizeRepository.Find(x => x.Name.Contains(categoryName));
            var categoryVms = new List<SizeVm>();

            foreach (var category in categories)
            {
                categoryVms.Add(mapper.Map<SizeVm>(category));
            }
            return categoryVms;
        }

        public async Task<bool> Update(SizeVm categoryVm)
        {
            try
            {
                var category = mapper.Map<Size>(categoryVm);

                this.unitOfWork.SizeRepository.Update(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}