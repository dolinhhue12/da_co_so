﻿using AutoMapper;
using Phone_Accessories.Data.Infastructures;
using Phone_Accessories.Models.Entity;
using Phone_Accessories.ViewModels.Colors;

namespace Phone_Accessories.Application.Colors
{
    public class ColorService : IColorService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public ColorService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<bool> Create(ColorVm categoryVm)
        {
            try
            {
                var category = mapper.Map<Color>(categoryVm);

                await this.unitOfWork.ColorRepository.Add(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                unitOfWork.ColorRepository.Delete(id);
                await unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<ColorVm>> GetAll()
        {
            var categories = await this.unitOfWork.ColorRepository.GetAll();
            var cagoryVms = new List<ColorVm>();

            foreach (var category in categories)
            {
                cagoryVms.Add(mapper.Map<ColorVm>(category));
            }
            return cagoryVms;
        }

        public async Task<ColorVm> GetById(int id)
        {
            var category = await unitOfWork.ColorRepository.GetById(id);
            var categoryVm = mapper.Map<ColorVm>(category);

            return categoryVm;
        }

        public async Task<IList<ColorVm>> Search(string categoryName)
        {
            var categories = await this.unitOfWork.ColorRepository.Find(x => x.Name.Contains(categoryName));
            var categoryVms = new List<ColorVm>();

            foreach (var category in categories)
            {
                categoryVms.Add(mapper.Map<ColorVm>(category));
            }
            return categoryVms;
        }

        public async Task<bool> Update(ColorVm categoryVm)
        {
            try
            {
                var category = mapper.Map<Color>(categoryVm);

                this.unitOfWork.ColorRepository.Update(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}