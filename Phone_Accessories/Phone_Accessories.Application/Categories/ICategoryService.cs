﻿using Phone_Accessories.ViewModels.Categories;

namespace Phone_Accessories.Application.Categories
{
    public interface ICategoryService
    {
        Task<IList<CategoryVm>> GetAll();

        Task<bool> Create(CategoryVm categoryVm);

        Task<CategoryVm> GetById(int id);

        Task<bool> Update(CategoryVm categoryVm);

        Task<bool> Delete(int id);

        Task<IList<CategoryVm>> Search(string categoryName);
    }
}