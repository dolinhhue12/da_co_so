﻿using Phone_Accessories.ViewModels.EntityBaseVms;

namespace Phone_Accessories.ViewModels.Colors
{
    public class ColorVm : EntityBaseVm
    {
        public int Id { get; set; }

        public string? Name { get; set; }
    }
}