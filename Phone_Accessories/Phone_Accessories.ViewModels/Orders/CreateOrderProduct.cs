﻿namespace Phone_Accessories.ViewModels.Orders
{
    public class CreateOrderProduct
    {
        public int Id { get; set; }

        public int Quantity { get; set; }
    }
}