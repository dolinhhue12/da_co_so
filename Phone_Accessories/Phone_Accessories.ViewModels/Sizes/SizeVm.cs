﻿using Phone_Accessories.ViewModels.EntityBaseVms;

namespace Phone_Accessories.ViewModels.Sizes
{
    public class SizeVm : EntityBaseVm
    {
        public int Id { get; set; }

        public string? Name { get; set; }
    }
}