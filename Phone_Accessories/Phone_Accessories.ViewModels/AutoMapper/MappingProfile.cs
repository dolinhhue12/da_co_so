﻿using AutoMapper;
using Phone_Accessories.Models.Entity;
using Phone_Accessories.ViewModels.Categories;
using Phone_Accessories.ViewModels.Colors;
using Phone_Accessories.ViewModels.Orders;
using Phone_Accessories.ViewModels.Products;
using Phone_Accessories.ViewModels.Sizes;

namespace Phone_Accessories.ViewModels.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Category, CategoryVm>().ReverseMap();
            CreateMap<Size, SizeVm>().ReverseMap();
            CreateMap<Color, ColorVm>().ReverseMap();
            CreateMap<Product, ViewProduct>().ReverseMap();
            CreateMap<Product, CreateProduct>().ReverseMap();
            CreateMap<Order, CreateOrder>().ReverseMap();
            CreateMap<Order, ViewOrder>().ReverseMap();
            CreateMap<Order, ViewOrderBasic>().ReverseMap();
        }
    }
}