﻿using Microsoft.AspNetCore.Mvc;
using Phone_Accessories.Application.Sizes;
using Phone_Accessories.ViewModels.Sizes;

namespace Phone_Accessories.WebApp.Areas.Admin.Controllers
{
    public class SizeController : BaseAdminController
    {
        private readonly ILogger<CategoryController> logger;
        private readonly ISizeService categoryService;

        public SizeController(ILogger<CategoryController> logger, ISizeService categoryService)
        {
            this.logger = logger;
            this.categoryService = categoryService;
        }
        public async Task<IActionResult> Index()
        {
            var categories = await categoryService.GetAll();

            return View(categories);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SizeVm categoryVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforCreate"] = "Create fail";
                return View(categoryVm);
            }

            var isSuccess = await categoryService.Create(categoryVm);

            if (isSuccess)
            {
                TempData["InforCreate"] = "Create success";
                return RedirectToAction("Index", "Size");
            }
            return View(categoryVm);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var categoryVm = await categoryService.GetById(id);

            if (categoryVm != null)
                return View(categoryVm);

            return RedirectToAction("Index", "Size");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(SizeVm categoryVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforUpdate"] = "Update fail";
                return View(categoryVm);
            }

            var isSuccess = await categoryService.Update(categoryVm);

            if (isSuccess)
            {
                TempData["InforUpdate"] = "Update success";
                return RedirectToAction("Index", "Size");
            }
            return View(categoryVm);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var categoryVm = await categoryService.GetById(id);

            if (categoryVm != null)
                return View(categoryVm);

            return RedirectToAction("Index", "Size");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id, IFormCollection collection)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "Delete fail";

            var isSuccess = await categoryService.Delete(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "Delete success";
                return RedirectToAction("Index", "Size");
            }

            return View();
        }
    }
}
