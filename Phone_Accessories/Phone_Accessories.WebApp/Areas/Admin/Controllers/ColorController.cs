﻿using Microsoft.AspNetCore.Mvc;
using Phone_Accessories.Application.Colors;
using Phone_Accessories.ViewModels.Colors;

namespace Phone_Accessories.WebApp.Areas.Admin.Controllers
{
    public class ColorController : BaseAdminController
    {
        private readonly ILogger<CategoryController> logger;
        private readonly IColorService categoryService;

        public ColorController(ILogger<CategoryController> logger, IColorService categoryService)
        {
            this.logger = logger;
            this.categoryService = categoryService;
        }
        public async Task<IActionResult> Index()
        {
            var categories = await categoryService.GetAll();

            return View(categories);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ColorVm categoryVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforCreate"] = "Create fail";
                return View(categoryVm);
            }

            var isSuccess = await categoryService.Create(categoryVm);

            if (isSuccess)
            {
                TempData["InforCreate"] = "Create success";
                return RedirectToAction("Index", "Color");
            }
            return View(categoryVm);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var categoryVm = await categoryService.GetById(id);

            if (categoryVm != null)
                return View(categoryVm);

            return RedirectToAction("Index", "Color");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ColorVm categoryVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforUpdate"] = "Update fail";
                return View(categoryVm);
            }

            var isSuccess = await categoryService.Update(categoryVm);

            if (isSuccess)
            {
                TempData["InforUpdate"] = "Update success";
                return RedirectToAction("Index", "Color");
            }
            return View(categoryVm);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var categoryVm = await categoryService.GetById(id);

            if (categoryVm != null)
                return View(categoryVm);

            return RedirectToAction("Index", "Color");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id, IFormCollection collection)
        {
            if (!ModelState.IsValid)
                TempData["InforDelete"] = "Delete fail";

            var isSuccess = await categoryService.Delete(id);

            if (isSuccess)
            {
                TempData["InforDelete"] = "Delete success";
                return RedirectToAction("Index", "Color");
            }

            return View();
        }
    }
}
