﻿using Microsoft.AspNetCore.Mvc;
using Phone_Accessories.Application.Orders;
using Phone_Accessories.ViewModels.Orders;

namespace Phone_Accessories.WebApp.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService orderService;

        public OrderController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        [HttpGet]
        public async Task<IActionResult> GetByUserId(string userId)
        {
            var order = await orderService.GetByUserId(userId);
            if (order != null)
                return View(order);

            return View();
        }

        [HttpPost]
        //[AllowAnonymous]
        public async Task<IActionResult> Create(string name, string phone, string address, string userId, IList<CreateOrderProduct> products)
        {
            if (!ModelState.IsValid)
                return View();

            var createOrder = new CreateOrder
            {
                UserId = userId,
                Address = address,
                Phone = phone,
                NameUser = name,
                Status = false,
                Products = products
            };

            bool isSuccess = await orderService.Create(createOrder);

            if (isSuccess)
                return RedirectToAction("GetByUserId", "Order", new { userId = createOrder.UserId });

            return BadRequest();
        }

       
        public async Task<IActionResult> Reject(int id, string userId)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = await orderService.Reject(id);

            if (isSuccess)
                return RedirectToAction("GetByUserId", new { userId = userId });

            return BadRequest();
        }

        public async Task<IActionResult> GetById(int id)
        {
            var order = await orderService.GetById(id);
            if (order != null)
                return View(order);

            return BadRequest();
        }
    }
}
